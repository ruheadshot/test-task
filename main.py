import getpass
import os
import time
from abc import ABC
from os.path import expanduser
from os import listdir
from os.path import isfile, join
from psutil import virtual_memory
import logging
import sys

# init logger
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    stream=sys.stdout
)
logger = logging.getLogger()


def loggable(func):
    # logging decorator

    def wrapper(self, *args, **kwargs):
        logger.info('| running method {0}'.format(func.__name__))
        res = func(self, *args, **kwargs)
        return res

    return wrapper


# custom exception class for better handling
class PreparationInterrupt(Exception):
    pass


# base abstract class for test cases
class TestCase(ABC):
    tc_id = 0
    name = 'unnamed'

    def prep(self):
        logger.info('| skipped method prep')

    def run(self):
        logger.info('| skipped method run')

    def clean_up(self):
        logger.info('| skipped method clean_up')

    def execute(self):
        logger.info('Running test case #{0} "{1}"'.format(self.tc_id, self.name))
        try:
            self.prep()
            self.run()
            self.clean_up()
        except PreparationInterrupt as err:
            logger.warning('Prevent run test case while prep, reason: {}'.format(err))
        except Exception as err:
            logger.error('Running test case error: {}'.format(err))
        finally:
            logger.info('| end of test case\n')


class FileList(TestCase):
    tc_id = 1
    name = 'File List'

    @loggable
    def prep(self):
        timestamp = int(time.time())
        if timestamp % 2 != 0:
            raise PreparationInterrupt(
                'Current unixtime ({}) isn\'t odd, breaking test case execution...'.format(timestamp))

    @loggable
    def run(self):
        # get current user home directory path
        home = expanduser("~")
        logger.info('|| getting current user ({}) home directory ({}) files list...'.format(getpass.getuser(), home))
        # get only files from directory using list generator
        files_list = [elem for elem in listdir(home) if isfile(join(home, elem))]
        files_list.sort()
        logger.info('|| files list:')
        for file in files_list:
            logger.info(' ├─ ' + file)


class RandomFile(TestCase):
    tc_id = 2
    name = 'Random File'
    filename = 'test'
    file_size_in_kb = 1024 ** 2
    full_path = None

    # get full path for file
    def _get_full_path(self):
        if self.full_path is None:
            self.full_path = os.path.join(os.getcwd(), self.filename)
        return self.full_path

    @loggable
    def prep(self):
        mem = virtual_memory()
        mem_in_gb = mem.total / 1024 ** 3
        logger.info('| Total physical memory: {}GB'.format(mem_in_gb))
        if mem_in_gb < 1:
            raise PreparationInterrupt(
                'Memory size less than 1GB (got {}GB), breaking test case execution...'.format(mem_in_gb))

    @loggable
    def run(self):
        logger.info(
            '|| creating file {} with random data and size {}KB'.format(self._get_full_path(),
                                                                        self.file_size_in_kb // 1024))
        # create file and open it in binary mode
        with open(self._get_full_path(), 'xb') as file_handler:
            file_handler.write(os.urandom(self.file_size_in_kb))

    @loggable
    def clean_up(self):
        logger.info('|| removing file {}'.format(self._get_full_path()))
        os.remove(self._get_full_path())


test_cases = [
    FileList(),
    RandomFile()
]

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    for test_case in test_cases:
        test_case.execute()
